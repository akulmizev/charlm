import tensorflow as tf
import numpy as np
from sklearn.utils import shuffle
from utils import *
import json

class charlm(object):

  def __init__(self, config, c2i, seq_len, vocab_size):

    f = open(config, "r")
    config = json.load(f)
    
    self.epochs = config["epochs"]
    self.batch_size = config["batch_size"]
    self.keep_rate = config["keep_rate"]
    self.num_layers = config["num_layers"]
    self.h_size = config["h_size"]
    self.embedding_dim = config["embed_dim"]
    self.learning_rate = config["learning_rate"]
    self.grad_clip = config["grad_clip"]
    if config["device"] == "gpu":
      self.device = "/gpu:0"
    else:
      self.device = "/cpu:0"
    self.sequence_length = seq_len
    self.vocab_size = vocab_size
    self.c2i = c2i

    self.x = tf.placeholder(tf.int32, [None, self.sequence_length])
    self.y = tf.placeholder(tf.int32, [None, self.sequence_length])
      
    def lstm(inputs):
      def make_cell():
        cell = tf.contrib.rnn.BasicLSTMCell(self.h_size,
                                            forget_bias=0.0)
        cell = tf.contrib.rnn.DropoutWrapper(cell,
                                             output_keep_prob=self.keep_rate)
        return cell

      cell = tf.contrib.rnn.MultiRNNCell(
        [make_cell() for _ in range(self.num_layers)], state_is_tuple=True)
      initial_state = cell.zero_state(self.batch_size, dtype=tf.float32)
      inputs = tf.unstack(inputs, num=self.sequence_length, axis=1)
      outputs, state = tf.nn.static_rnn(cell, inputs,
                                        initial_state=initial_state)
      outputs = tf.reshape(tf.concat(outputs, 1), [-1, self.h_size])
      return outputs, state

    with tf.device(self.device):
      self.E = tf.get_variable("embedding", [self.vocab_size, self.embedding_dim],
                               dtype=tf.float32)
      inputs = tf.nn.embedding_lookup(self.E, self.x)
      output, state = lstm(inputs)
      output = tf.nn.relu(output)
      W_out = tf.get_variable("softmax_w", [self.h_size, self.vocab_size],
                              dtype=tf.float32)
      b_out = tf.get_variable("softmax_b", [self.vocab_size], dtype=tf.float32)
      logits = tf.nn.xw_plus_b(output, W_out, b_out)
      self.probs = tf.nn.softmax(logits)
      self.logits = tf.reshape(logits, [self.batch_size, self.sequence_length,
                                        self.vocab_size])
      loss = tf.contrib.seq2seq.sequence_loss(
        self.logits,
        self.y,
        tf.ones([self.batch_size, self.sequence_length]),
        average_across_timesteps=False,
        average_across_batch=True)

      self.cost = tf.reduce_sum(loss)
      tvars = tf.trainable_variables()
      grads, _ = tf.clip_by_global_norm(tf.gradients(self.cost, tvars),
                                        self.grad_clip)
      with tf.name_scope('optimizer'):
        optimizer = tf.train.AdamOptimizer(self.learning_rate)
        self.train_op = optimizer.apply_gradients(zip(grads, tvars))

    self.init = tf.global_variables_initializer()
    self.sess = None

  def sample(self, prime="<", sample_len=300):
    def weighted_pick(weights):
      t = np.cumsum(weights)
      s = np.sum(weights)
      return(int(np.searchsorted(t, np.random.rand(1)*s)))

    ret_string = ""
    char = prime
    for n in range(sample_len):
      x_dummy = np.zeros((self.batch_size, self.sequence_length))
      x_dummy[0,0] = self.c2i[char]
      probs = self.sess.run([self.probs], feed_dict={self.x: x_dummy})
      p = probs[0][0]
      sample = weighted_pick(p)
      pred_char = i2c(self.c2i, sample)
      if pred_char == "<":
        ret_string += ""
      elif pred_char == ">":
        break
      else:
        ret_string += pred_char
        char = pred_char

    return ret_string
