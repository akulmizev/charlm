# charlm

A character-level LSTM language model based on [Andrej Karpathy's blog post](http://karpathy.github.io/2015/05/21/rnn-effectiveness/). 

Dependencies include Python 3.5+, Tensorflow 1.3+, scikit-learn, and nltk. 

Usage: `python train.py --data your_text.txt`. Model parameters can be adjusted in `config.json`. 

Support for NLTK's Project Gutenberg corpus is enabled by default, simply refer to one of the files mentioned here: 

```
'austen-emma.txt', 'austen-persuasion.txt', 'austen-sense.txt', 'bible-kjv.txt',
'blake-poems.txt', 'bryant-stories.txt', 'burgess-busterbrown.txt',
'carroll-alice.txt', 'chesterton-ball.txt', 'chesterton-brown.txt',
'chesterton-thursday.txt', 'edgeworth-parents.txt', 'melville-moby_dick.txt',
'milton-paradise.txt', 'shakespeare-caesar.txt', 'shakespeare-hamlet.txt',
'shakespeare-macbeth.txt', 'whitman-leaves.txt'
```