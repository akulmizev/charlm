from collections import defaultdict
import numpy as np
from nltk.corpus import gutenberg
from nltk import sent_tokenize
from sklearn.utils import shuffle

def get_avg_len(sents):
    lengths = [len(i) for i in sents]
    return int((sum(lengths)) / len(lengths))

def get_text(text='whitman-leaves.txt'):

    if text in gutenberg.fileids():
        sents = [" ".join(sent) for sent in gutenberg.sents(text)]
    else:
        f = open(text, "r")
        raw = f.read()
        sents = sent_tokenize(raw)
        
    sents = shuffle(sents)
    avg_len = get_avg_len(sents)

    return sents, avg_len

def proc_text(text='whitman-leaves.txt'):
    sents, max_len = get_text(text)
    
    c2i = defaultdict(lambda: len(c2i))
    c2i["<"] = 0
    c2i[">"] = 1

    chars_x = np.zeros((len(sents), max_len+1))
    chars_y = np.zeros((len(sents), max_len+1))
    for i, sent in enumerate(sents):
        chars_x[i][0] = c2i["<"]
        for j, char in enumerate(sent):
            if j >= max_len:
                break
            if j+1 == len(sent):
                chars_y[i][j+1] = c2i[">"]
            chars_x[i][j+1] = c2i[char]
            chars_y[i][j] = c2i[char]

    return (chars_x, chars_y), c2i, max_len

def i2c(c2i, i):
    return list(c2i.keys())[list(c2i.values()).index(i)]
