import os
import argparse
import tensorflow as tf
import numpy as np
from sklearn.utils import shuffle
from utils import *
from model import *

def main():
    parser = argparse.ArgumentParser(
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('--data', type=str, default='whitman-leaves.txt',
                        help='data directory containing input')
    parser.add_argument('--save_dir', type=str, default='save',
                        help='directory to store checkpointed models')
    parser.add_argument('--save_step', type=int, default=100,
                        help='save model after n iterations')
    parser.add_argument('--config_file', type=str, default='config.json',
                        help='path to config file')
    parser.add_argument('--display_step', type=int, default=10,
                        help='display results after n epochs')
    args = parser.parse_args()
    train(args)

def get_minibatch(x, y, start_index, end_index):
    indices = range(start_index, end_index)
    x = [x[i] for i in indices]
    y = [y[i] for i in indices]

    return x, y
    
def train(args):
    if args.save_dir is not None:
        assert os.path.isdir(args.save_dir), " %s must be a a path" % args.save_dir

    print("Processing %s..." % args.data)
    text, c2i, max_len = proc_text(args.data)
    print("Got {} sents and {} characters.".format(text[0].shape[0], len(c2i)))
    print("Max sentence length is {}".format(max_len))
    model = charlm(args.config_file, seq_len=text[0].shape[1], vocab_size=len(c2i), c2i=c2i)
  
    model.sess = tf.Session()
    model.sess.run(model.init)
    saver = tf.train.Saver()
    print('Training...')
    
    for epoch in range(model.epochs):
        x, y = shuffle(text[0], text[1])
        avg_cost = 0.
        total_batch = int(len(x) / model.batch_size)

        for i in range(total_batch):
            x_batch,y_batch = get_minibatch(x, y, model.batch_size * i,
                                            model.batch_size * (i + 1))
            _, c = model.sess.run([model.train_op, model.cost],
                                  feed_dict={model.x: x_batch,
                                             model.y: y_batch})
            avg_cost += c / (total_batch * model.batch_size)

        if (epoch+1) % args.display_step == 0:
            print("Epoch:", (epoch+1), "Cost:", avg_cost)
            print(model.sample(sample_len=150))
        if (epoch+1) % args.save_step == 0:
            saver.save(model.sess, args.save_dir+'/test',global_step=epoch)

if __name__ == '__main__':
    main()
